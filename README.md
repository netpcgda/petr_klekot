## PHP - Task # 1 (required)
Please write a simple application that manages a contact list.
Functionality:

1. Login
2. View contact list (available to every user) The list should contain basic data. After selecting a particular contact, display his details.
3. Edit and delete existing entries and add new ones (available only to a logged in user)

A single contact should have at least:

1. name,
2. surname,
3. email - unique,
4. phone number,
5. date of birth.

Technical assumptions:

1. The application should be written minimum with PHP5.5 using the MySQL 5 database
2. We suggest that validation of forms takes place without reloading the page.
3. The use of Symfony 3 components (Symfony 2 allowed) or Laravel is preferred but not required
4. Other free libraries may be used.
5. Pay attention to application security.
6. Appearance of the application is not really important.

Files, ERD schema and database dump should be sent by e-mail in a zipped archive
(7-zip or ZIP) to i.zhariy@npc.pl
---
## SQL / ERD - Task 2 (additional)
Design the database described below.

1. Draw an ERD diagram, mark the keys and foreign keys.
2. Next show the content of SQL queries returning the requested data below.
3. Point the columns, which you need to index to speed up your queries.
4. Use only SQL.

Database description:
The database contains information about persons (born after 1900) such as: name, surname, date of birth,
gender, earnings. Anyone can have a mother and / or father. A woman may have one husband, a man may
have one wife. People work in companies with names. One person can work in several companies
simultaneously, as an employee or subcontractor. Each company has exactly one CEO.
Requested data:

1. Find the name of the person with the greatest number of grandchildren.
3. Find the average number of employees and the average number of subcontractors in all companies.
3. Find a family (max two generations) with the least earnings. List the name of any person in this family.

Hint: A 1st generation family is an X person with a possible spouse. A 2nd generation family is a one generatio
family with all its children (including possible spouses) or parents.

---
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).